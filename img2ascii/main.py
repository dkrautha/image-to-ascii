import argparse
import sys

from img2ascii.lib import run
from img2ascii import __version__


def create_parser():
    parser = argparse.ArgumentParser(
        prog="img2ascii",
        description="Cli program for generating ascii art from an input image")

    parser.add_argument("-v", "--version",
                        action=("version"),
                        version=__version__,
                        help=("show version of img2ascii"))

    parser.add_argument("input_path",
                        type=str,
                        help="location of file to load from")

    parser.add_argument("output_path",
                        type=argparse.FileType("w", encoding=("UTF-8")),
                        help="location of output file")

    parser.add_argument("-c",
                        "--columns",
                        dest="columns",
                        type=int,
                        help="number of columns of the final image",
                        default=80)

    parser.add_argument("-s",
                        "--scale",
                        dest="scale",
                        type=float,
                        help="font scaling value",
                        default=0.43)

    parser.add_argument("--clr",
                        "--color",
                        dest="color",
                        action="store_true",
                        help="output file will be in color")

    parser.add_argument("--html",
                        dest="html",
                        action="store_true",
                        help="output file will be formatted as html")

    return parser


def main():
    parser = create_parser()
    if len(sys.argv) <= 1:
        args = parser.parse_args(['-h'])
    else:
        args = parser.parse_args()

    run(args.input_path,
        args.output_path,
        args.columns,
        args.scale,
        args.color,
        args.html)


if __name__ == "__main__":
    main()
