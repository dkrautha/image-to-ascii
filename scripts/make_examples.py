import subprocess
import sys

def main():
    paths = ["examples/anime.jpeg", "examples/map.jpeg", "examples/skyline.jpg"]

    try:
        for path in paths:
            starter = path.split(".jp")[0]
            print(starter)
            subprocess.call(["poetry", "run", "img2ascii", f"{path}", f"{starter}_text.txt", "-c", "250", "--clr"])
            subprocess.call(["poetry", "run", "img2ascii", f"{path}", f"{starter}_html.html", "-c", "250", "--clr", "--html"])

    except subprocess.CalledProcessError:
        print("error")
        sys.exit(1)

if __name__ == '__main__':
    main()
