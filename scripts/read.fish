#!/usr/bin/env fish

for line in (cat < $argv)
    echo -e $line
end
