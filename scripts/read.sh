#!/usr/bin/env bash
set -euo pipefail


while read -r line; do echo -e $line; done < "$1"
